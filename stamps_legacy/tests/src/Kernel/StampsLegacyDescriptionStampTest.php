<?php

declare(strict_types = 1);

namespace Drupal\Tests\stamps\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\file\FileRepositoryInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\media\Entity\Media;
use Drupal\sm\QueueInterceptor\SmLegacyDrupalQueueItem;
use Drupal\sm\QueueInterceptor\SmLegacyQueueFactory;
use Drupal\stamps\Messenger\Stamp\DescriptionStamp;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Symfony\Component\DependencyInjection\ServiceLocator;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Tests describer middleware.
 *
 * @covers \Drupal\stamps\Messenger\Stamp\DescriptionStamp
 * @coversDefaultClass \Drupal\stamps_legacy\Messenger\Middleware\StampsLegacyMessageDescriberMiddleware
 */
final class StampsLegacyDescriptionStampTest extends KernelTestBase {

  use MediaTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'stamps',
    'stamps_legacy',
    'stamps_test',
    'sm',
    'sm_test_transport',
    'media',
    'system',
    'file',
    'image',
    'field',
    'user',
  ];

  /**
   * @var null|array
   */
  private ?array $messageToSendersMapping = NULL;

  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['system']);
    $this->installEntitySchema('file');
    $this->installEntitySchema('user');
    $this->installEntitySchema('media');
    $this->installSchema('file', ['file_usage']);

    // Turn on legacy queue interception.
    $this->setSetting('queue_default', SmLegacyQueueFactory::class);
  }

  /**
   * Tests stamp is applied for media thumbnail queue item.
   *
   * Test relies on 'Queued thumbnails' turned on at media type page.
   * e.g. on admin/structure/media/manage/MEDIATYPE.
   *
   * @see \Drupal\media\Plugin\QueueWorker\ThumbnailDownloader
   */
  public function testThumbnailDownloader(): void {
    $this->setRouting([
      '*' => 'sm_test_transport__in_memory',
    ]);

    $this->createMediaType(
      'image', [
        'id' => 'test',
        'label' => 'Test',
        'queue_thumbnail_downloads' => TRUE,
      ],
    );

    $fileName = mt_rand(10000, 99999) . '.image';
    $file = $this->fileRepository()->writeData('Foo!', 'public://' . $fileName);

    Media::create([
      'bundle' => 'test',
      'field_media_image' => $file,
    ])->save();

    /** @var \Symfony\Component\Messenger\Transport\InMemory\InMemoryTransport $transport */
    $transport = $this->receiverLocator()->get('sm_test_transport__in_memory');
    $envelopes = $transport->get();
    $all = [];
    foreach ($envelopes as $envelope) {
      $all[] = $this->bus()->dispatch($envelope);
    }

    static::assertCount(1, $all);
    $envelope = $all[0];
    $message = $envelope->getMessage();
    static::assertInstanceOf(SmLegacyDrupalQueueItem::class, $message);

    $stamp = $envelope->last(DescriptionStamp::class);
    static::assertNotNull($stamp);

    static::assertEquals('Thumbnail fetched', $stamp->getTitle());
    static::assertEquals('Generated thumbail for ' . $fileName, $stamp->getDescription());
  }

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    // This method can be invoked again when calling kernel->rebuildContainer
    // a strategy borrowed from `DrupalFlushAllCachesTest`.
    parent::register($container);

    if (NULL !== $this->messageToSendersMapping) {
      $container->setParameter('sm.routing', $this->messageToSendersMapping);
    }
  }

  /**
   * Sets routing and triggers a rebuild.
   *
   * @phpstan-param array<string|class-string, string|string[]> $messageToSendersMapping
   */
  private function setRouting(array $messageToSendersMapping): void {
    $this->messageToSendersMapping = $messageToSendersMapping;
    \Drupal::service('kernel')->rebuildContainer();
  }

  private function bus(): MessageBusInterface {
    return \Drupal::service(MessageBusInterface::class);
  }

  private function receiverLocator(): ServiceLocator {
    return \Drupal::service('messenger.receiver_locator');
  }

  private function fileRepository(): FileRepositoryInterface {
    return \Drupal::service('file.repository');
  }

}
