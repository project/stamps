<?php

declare(strict_types = 1);

namespace Drupal\stamps\Messenger\Stamp;

use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Messenger\Stamp\StampInterface;

/**
 * Represents the user logged in when the message was dispatched to a bus.
 */
final class CurrentUserStamp implements StampInterface {

  /**
   * @var int<0, max>
   */
  private int $userId;

  /**
   * @param int<0, max> $userId
   */
  private function __construct(int $userId) {
    $this->userId = $userId;
  }

  /**
   * Creates a stamp from a user session.
   */
  public static function fromAccount(AccountInterface $account): static {
    // This can return string-ints.
    // @phpstan-ignore-next-line
    $uid = (int) $account->id();
    if ($uid < 0) {
      throw new \LogicException();
    }

    return new static($uid);
  }

  /**
   * @return int<0, max>
   */
  public function getUserId(): int {
    return $this->userId;
  }

}
