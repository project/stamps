<?php

declare(strict_types = 1);

namespace Drupal\stamps_test;

/**
 * @see \Drupal\stamps_test\Messenger\StampsTestMessageHandler
 */
final class StampsTestMessage {

  /**
   * Creates a new StampsTestMessage.
   */
  public function __construct(
    public ?string $handledBy = NULL,
  ) {
  }

}
