<?php

declare(strict_types = 1);

namespace Drupal\stamps;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

/**
 * Service provider for Stamps.
 */
final class StampsServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    $container
      // 100 is before MessengerPass.
      ->addCompilerPass(new StampsCompilerPass(), priority: 100);
  }

}
