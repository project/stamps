<?php

declare(strict_types = 1);

namespace Drupal\stamps_test\Messenger;

use Drupal\stamps_test\StampsTestMessage;

/**
 * Message handler.
 */
final class StampsTestMessageHandler {

  /**
   * Message handler for StampsTestMessage messages.
   */
  public function __invoke(StampsTestMessage $message): void {
    $message->handledBy = __METHOD__;
  }

}
