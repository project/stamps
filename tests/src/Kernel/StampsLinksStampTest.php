<?php

declare(strict_types = 1);

namespace Drupal\Tests\stamps\Kernel;

use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Drupal\stamps\Messenger\Stamp\LinksStamp;
use Drupal\stamps_test\StampsTestMessage;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Tests Links stamp.
 *
 * @covers \Drupal\stamps\Messenger\Stamp\LinksStamp
 */
final class StampsLinksStampTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'stamps',
    'stamps_test',
    'sm',
    'system',
  ];

  /**
   * Tests the stamp.
   */
  public function testStamp(): void {
    $link1 = Link::fromTextAndUrl('plain string', Url::fromRoute('system.status'));
    $link2 = Link::fromTextAndUrl(new TranslatableMarkup('markup obj'), Url::fromUri('http://example.com/foo/bar?baz=123'));
    $stamp = LinksStamp::fromLinks($link1, $link2);

    $message = new Envelope(new StampsTestMessage(), [$stamp]);
    $envelope = $this->bus()->dispatch($message);

    // Comes out the other side unscathed.
    $stamp = $envelope->last(LinksStamp::class);
    static::assertInstanceOf(LinksStamp::class, $stamp);

    $result = $stamp->getLinks();
    static::assertCount(2, $result);
    static::assertEquals('plain string', $result[0]->getText());
    static::assertEquals('markup obj', $result[1]->getText());
    static::assertEquals('/admin/reports/status', $result[0]->getUrl()->toString());
    static::assertEquals('http://example.com/foo/bar?baz=123', $result[1]->getUrl()->toString());
  }

  /**
   * Default messenger bus.
   */
  private function bus(): MessageBusInterface {
    return \Drupal::service(MessageBusInterface::class);
  }

}
