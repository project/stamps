<?php

declare(strict_types = 1);

namespace Drupal\stamps_legacy\Messenger\Middleware;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\media\MediaStorage;
use Drupal\sm\QueueInterceptor\SmLegacyDrupalQueueItem;
use Drupal\stamps\Messenger\Stamp\ActionsStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;

/**
 * Adds links to contrib queue items.
 *
 * @see \Drupal\stamps\Messenger\Stamp\LinksStamp
 */
final class StampsLegacyMessageLinksMiddleware implements MiddlewareInterface {

  public function __construct(
    public \Closure $entityTypeManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Envelope $envelope, StackInterface $stack): Envelope {
    if (NULL === $envelope->last(ActionsStamp::class)) {
      $message = $envelope->getMessage();
      if ($message instanceof SmLegacyDrupalQueueItem) {
        $stamp = $this->getActionStamp($message->queueName, $message->data);
        $envelope = $envelope->with(...array_filter([$stamp]));
      }
    };

    return $stack->next()->handle($envelope, $stack);
  }

  private function getActionStamp(string $queueName, mixed $data): ?ActionsStamp {
    return match ($queueName) {
      // \Drupal\media\Plugin\QueueWorker\ThumbnailDownloader.
      'media_entity_thumbnail' => (function (mixed $data): ?ActionsStamp {
        $media = $this->mediaStorage()->load($data['id']);
        if ($media === NULL) {
          return NULL;
        }

        $url = $media->toUrl('edit-form');
        return ActionsStamp::fromLinks(
          primary: [Link::fromTextAndUrl(
              sprintf('View %s', (string) $media->getEntityType()->getSingularLabel()),
              $url,
            ),
          ],
        );
      })($data),
      default => NULL,
    };
  }

  private function mediaStorage(): MediaStorage {
    /** @var \Drupal\media\MediaStorage */
    return $this->entityTypeManager()->getStorage('media');
  }

  private function entityTypeManager(): EntityTypeManagerInterface {
    return ($this->entityTypeManager)();
  }

}
