<?php

declare(strict_types = 1);

namespace Drupal\stamps\Messenger\Middleware;

use Drupal\Core\Session\AccountInterface;
use Drupal\stamps\Messenger\Stamp\CurrentUserStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;

/**
 * Logs the current user if messages are dispatched with an active session.
 *
 * @see \Drupal\stamps\Messenger\Stamp\CurrentUserStamp
 */
final class StampsCurrentUserStampMiddleware implements MiddlewareInterface {

  /**
   * Constructor.
   */
  public function __construct(
    private AccountInterface $currentUser,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Envelope $envelope, StackInterface $stack): Envelope {
    if (NULL === $envelope->last(CurrentUserStamp::class)) {
      $envelope = $envelope->with(
        CurrentUserStamp::fromAccount($this->currentUser),
      );
    }

    return $stack->next()->handle($envelope, $stack);
  }

}
