<?php

declare(strict_types = 1);

namespace Drupal\stamps\Messenger\Stamp;

use Drupal\Core\Link;
use Symfony\Component\Messenger\Stamp\StampInterface;

/**
 * Links stamp.
 */
final class LinksStamp implements StampInterface {

  /**
   * Constructor.
   *
   * @param non-empty-array<\Drupal\Core\Link> $links
   *
   * @internal
   *   The data structure of this stamp may change at any time.
   */
  private function __construct(
    private array $links,
  ) {
  }

  /**
   * Creates a LinksStamp from one or more Links.
   *
   * @phpstan-param non-empty-list<\Drupal\Core\Link> ...$links
   */
  public static function fromLinks(Link ...$links): static {
    if (count($links) === 0) {
      throw new \LogicException('At least one link must be provided.');
    }

    return new static($links);
  }

  /**
   * @phpstan-return non-empty-array<\Drupal\Core\Link>
   */
  public function getLinks(): iterable {
    return $this->links;
  }

}
