<?php

declare(strict_types = 1);

namespace Drupal\Tests\stamps\Kernel;

use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Drupal\stamps\Messenger\Enum\ActionLinkClass;
use Drupal\stamps\Messenger\Stamp\ActionsStamp;
use Drupal\stamps_test\StampsTestMessage;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Tests Actions stamp.
 *
 * @covers \Drupal\stamps\Messenger\Stamp\ActionsStamp
 */
final class StampsActionsStampTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'stamps',
    'stamps_test',
    'sm',
    'system',
  ];

  /**
   * Tests the stamp.
   */
  public function testStamp(): void {
    $link1 = Link::fromTextAndUrl('plain string', Url::fromRoute('system.status'));
    $link2 = Link::fromTextAndUrl(new TranslatableMarkup('markup obj'), Url::fromUri('http://example.com/foo/bar?baz=123'));
    $stamp = ActionsStamp::fromLinks(
      primary: [$link1],
      danger: [$link2],
    );

    $message = new Envelope(new StampsTestMessage(), [$stamp]);
    $envelope = $this->bus()->dispatch($message);

    // Comes out the other side unscathed.
    $stamp = $envelope->last(ActionsStamp::class);
    static::assertInstanceOf(ActionsStamp::class, $stamp);

    // Get all links (no filter args).
    $result = iterator_to_array($stamp->getLinks());
    static::assertCount(2, $result);

    [$link0, $action0] = $result[0];
    [$link1, $action1] = $result[1];

    static::assertEquals(ActionLinkClass::PRIMARY, $action0);
    static::assertEquals(ActionLinkClass::DANGER, $action1);

    static::assertEquals('plain string', $link0->getText());
    static::assertEquals('markup obj', $link1->getText());
    static::assertEquals('/admin/reports/status', $link0->getUrl()->toString());
    static::assertEquals('http://example.com/foo/bar?baz=123', $link1->getUrl()->toString());

    // Get all links (with filter args).
    $result = iterator_to_array($stamp->getLinks([ActionLinkClass::DANGER]));
    static::assertCount(1, $result);
    [$link0, $action0] = $result[0];
    static::assertEquals('markup obj', $link0->getText());
    static::assertEquals(ActionLinkClass::DANGER, $action0);
  }

  /**
   * Default messenger bus.
   */
  private function bus(): MessageBusInterface {
    return \Drupal::service(MessageBusInterface::class);
  }

}
