<?php

declare(strict_types = 1);

namespace Drupal\stamps\Messenger\Stamp;

use Symfony\Component\Messenger\Stamp\StampInterface;

/**
 * Describes the message.
 */
final class DescriptionStamp implements StampInterface {

  private function __construct(
    private string $title,
    private ?string $description,
  ) {
  }

  public static function create(
    string $title,
    ?string $description = NULL,
  ): static {
    return new static($title, $description);
  }

  public function getTitle(): string {
    return $this->title;
  }

  public function getDescription(): ?string {
    return $this->description;
  }

}
