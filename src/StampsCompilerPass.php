<?php

declare(strict_types = 1);

namespace Drupal\stamps;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;

/**
 * Stamps compiler pass.
 */
final class StampsCompilerPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container): void {
    try {
      // @todo do not tie to param, use a proper locator.
      /** @var array $buses */
      $buses = $container->getParameter('sm.buses');
    }
    catch (ParameterNotFoundException) {
      $buses = [];
    }

    foreach ($buses as $id => $bus) {
      $buses[$id]['middleware'][] = ['id' => 'stamps_current_user_stamp'];
    }
    $container->setParameter('sm.buses', $buses);
  }

}
