<?php

declare(strict_types = 1);

namespace Drupal\stamps_legacy;

use Symfony\Component\DependencyInjection\Argument\ServiceClosureArgument;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Bus compiler pass.
 */
final class StampsLegacyCompilerPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container): void {
    try {
      // @todo do not tie to param, use a proper locator.
      /** @var array $buses */
      $buses = $container->getParameter('sm.buses');
    }
    catch (ParameterNotFoundException) {
      $buses = [];
    }

    foreach ($buses as $id => $bus) {
      $buses[$id]['middleware'][] = ['id' => 'stamps_legacy_message_describer'];
      $buses[$id]['middleware'][] = ['id' => 'stamps_legacy_message_links'];
    }
    $container->setParameter('sm.buses', $buses);

    // https://symfony.com/doc/current/service_container.html#injecting-a-closure-as-an-argument
    // https://symfony.com/doc/current/service_container/autowiring.html#generate-closures-with-autowiring
    // A service closure is an anonymous function that returns a service. This
    // type of instanciation is handy when you are dealing with lazy-loading.
    $definition = $container->getDefinition('messenger.middleware.stamps_legacy_message_describer');
    $definition->setArgument('$entityTypeManager', new ServiceClosureArgument(new Reference('entity_type.manager')));

    $definition = $container->getDefinition('messenger.middleware.stamps_legacy_message_links');
    $definition->setArgument('$entityTypeManager', new ServiceClosureArgument(new Reference('entity_type.manager')));
  }

}
