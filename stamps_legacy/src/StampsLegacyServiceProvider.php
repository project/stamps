<?php

declare(strict_types = 1);

namespace Drupal\stamps_legacy;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

/**
 * Service provider for Stamps Legacy.
 */
final class StampsLegacyServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    $container
      // 100 is before MessengerPass.
      ->addCompilerPass(new StampsLegacyCompilerPass(), priority: 100);
  }

}
