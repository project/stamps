<?php

declare(strict_types = 1);

namespace Drupal\stamps\Messenger\Stamp;

use Drupal\stamps\Messenger\Enum\ActionLinkClass;
use Symfony\Component\Messenger\Stamp\StampInterface;

/**
 * Action links stamp.
 */
final class ActionsStamp implements StampInterface {

  /**
   * Constructor.
   *
   * @phpstan-param array<value-of<ActionLinkClass>, \Drupal\Core\Link[]> $links
   *
   * @internal
   *   The data structure of this stamp may change at any time.
   */
  private function __construct(
    private array $links,
  ) {
  }

  /**
   * Creates a ActionsStamp from one or more Links.
   *
   * Future order of arguments is not guaranteed. Always use named arguments
   * when calling.
   *
   * Example usage:
   * <code>
   *   ActionsStamp::fromLinks(
   *     primary: [$link],
   *     negative: [$link, $link],
   *     positive: [$link],
   *   );
   * </code>
   *
   * @phpstan-param \Drupal\Core\Link[] $primary
   * @phpstan-param \Drupal\Core\Link[] $secondary
   * @phpstan-param \Drupal\Core\Link[] $danger
   * @phpstan-param \Drupal\Core\Link[] $positive
   * @phpstan-param \Drupal\Core\Link[] $negative
   */
  public static function fromLinks(
    array $primary = [],
    array $secondary = [],
    array $danger = [],
    array $positive = [],
    array $negative = [],
  ): static {
    /** @var array<value-of<ActionLinkClass>, \Drupal\Core\Link[]> $links */
    $links = [];
    $links[ActionLinkClass::PRIMARY->value] = $primary;
    $links[ActionLinkClass::SECONDARY->value] = $secondary;
    $links[ActionLinkClass::DANGER->value] = $danger;
    $links[ActionLinkClass::POSITIVE->value] = $positive;
    $links[ActionLinkClass::NEGATIVE->value] = $negative;

    // Remove empty.
    $links = array_filter($links);

    return new static($links);
  }

  /**
   * Get the links for this stamp.
   *
   * Example usage:
   * <code>
   *   $actionStamp->getLinks(filter: [ActionLinkClass::PRIMARY, ActionLinkClass::NEGATIVE])
   * </code>
   *
   * @phpstan-param array<\Drupal\stamps\Messenger\Enum\ActionLinkClass> $filter
   * @phpstan-return \Generator<array{\Drupal\Core\Link, \Drupal\stamps\Messenger\Enum\ActionLinkClass}>
   */
  public function getLinks(array $filter = []): \Generator {
    $links = $this->doGetLinks();
    if (count($filter) === 0) {
      yield from $links;
      return;
    }

    foreach ($links as [$link, $linkClass]) {
      if (in_array($linkClass, $filter, TRUE)) {
        yield [$link, $linkClass];
      }
    }
  }

  /**
   * @phpstan-return \Generator<array{\Drupal\Core\Link, \Drupal\stamps\Messenger\Enum\ActionLinkClass}>
   */
  private function doGetLinks(): \Generator {
    foreach (ActionLinkClass::cases() as $case) {
      foreach ($this->links[$case->value] ?? [] as $link) {
        yield [$link, $case];
      }
    }
  }

}
