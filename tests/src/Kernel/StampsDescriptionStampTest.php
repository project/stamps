<?php

declare(strict_types = 1);

namespace Drupal\Tests\stamps\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\stamps\Messenger\Stamp\DescriptionStamp;
use Drupal\stamps_test\StampsTestMessage;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Tests Description stamp.
 *
 * @covers \Drupal\stamps\Messenger\Stamp\DescriptionStamp
 */
final class StampsDescriptionStampTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'stamps',
    'stamps_test',
    'sm',
  ];

  /**
   * Tests the stamp.
   */
  public function testStamp(): void {
    $stamp = DescriptionStamp::create(
      title: 'foobar',
      description: 'baz',
    );

    $message = new Envelope(new StampsTestMessage(), [$stamp]);
    $envelope = $this->bus()->dispatch($message);

    // Comes out the other side unscathed.
    $stamp = $envelope->last(DescriptionStamp::class);
    static::assertInstanceOf(DescriptionStamp::class, $stamp);

    static::assertEquals('foobar', $stamp->getTitle());
    static::assertEquals('baz', $stamp->getDescription());
  }

  /**
   * Default messenger bus.
   */
  private function bus(): MessageBusInterface {
    return \Drupal::service(MessageBusInterface::class);
  }

}
