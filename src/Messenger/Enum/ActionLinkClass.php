<?php

declare(strict_types = 1);

namespace Drupal\stamps\Messenger\Enum;

/**
 * Represents classes of links for ActionStamp.
 *
 * @see \Drupal\stamps\Messenger\Stamp\ActionsStamp
 */
enum ActionLinkClass: string {

  case PRIMARY = 'primary';
  case SECONDARY = 'secondary';
  case DANGER = 'danger';
  case POSITIVE = 'positive';
  case NEGATIVE = 'negative';
}
