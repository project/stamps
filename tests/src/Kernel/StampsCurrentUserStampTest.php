<?php

declare(strict_types = 1);

namespace Drupal\Tests\stamps\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\stamps\Messenger\Stamp\CurrentUserStamp;
use Drupal\stamps_test\StampsTestMessage;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Tests Current User Stamp.
 *
 * @covers \Drupal\stamps\Messenger\Stamp\CurrentUserStamp
 * @covers \Drupal\stamps\Messenger\Middleware\StampsCurrentUserStampMiddleware
 */
final class StampsCurrentUserStampTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'stamps',
    'stamps_test',
    'sm',
    'user',
    'system',
  ];

  /**
   * Tests an authenticated user.
   */
  public function testUnauthenticated(): void {
    $message = new StampsTestMessage();
    $envelope = $this->bus()->dispatch($message);

    $currentUserStamp = $envelope->last(CurrentUserStamp::class);
    static::assertNotNull($currentUserStamp);
    static::assertEquals(0, $currentUserStamp->getUserId());
  }

  /**
   * Tests an authenticated user.
   */
  public function testAuthenticated(): void {
    $user = $this->setUpCurrentUser();

    // Make sure we didn't get an anonymous user.
    static::assertGreaterThan(0, $user->id());

    $message = new StampsTestMessage();
    $envelope = $this->bus()->dispatch($message);

    $currentUserStamp = $envelope->last(CurrentUserStamp::class);
    static::assertNotNull($currentUserStamp);
    static::assertEquals($user->id(), $currentUserStamp->getUserId());
  }

  /**
   * Default messenger bus.
   */
  private function bus(): MessageBusInterface {
    return \Drupal::service(MessageBusInterface::class);
  }

}
