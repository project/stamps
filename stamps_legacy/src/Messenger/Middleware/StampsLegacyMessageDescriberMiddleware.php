<?php

declare(strict_types = 1);

namespace Drupal\stamps_legacy\Messenger\Middleware;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\media\MediaStorage;
use Drupal\sm\QueueInterceptor\SmLegacyDrupalQueueItem;
use Drupal\stamps\Messenger\Stamp\DescriptionStamp;
use Symfony\Component\Mailer\Messenger\SendEmailMessage;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Message;

/**
 * Adds descriptions to core/contrib queue items.
 *
 * @see \Drupal\stamps\Messenger\Stamp\DescriptionStamp
 */
final class StampsLegacyMessageDescriberMiddleware implements MiddlewareInterface {

  public function __construct(
    public \Closure $entityTypeManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Envelope $envelope, StackInterface $stack): Envelope {
    if (NULL === $envelope->last(DescriptionStamp::class)) {
      $message = $envelope->getMessage();
      if ($message instanceof SmLegacyDrupalQueueItem) {
        $stamp = $this->getStampForQueue($message->queueName, $message->data);
        $envelope = $envelope->with(...array_filter([$stamp]));
      }
      elseif ($message instanceof SendEmailMessage) {
        $email = $message->getMessage();
        $description = '';
        if ($email instanceof Message) {
          $address = $email->getHeaders()->get('to')?->getBody()[0] ?? NULL;
          if ($address instanceof Address) {
            $name = $address->getName();
            $description = strlen($name) > 0 ? ((string) \t('Sent email to @name', ['@name' => $name])) : '';
          }
        }
        $envelope = $envelope->with(
          DescriptionStamp::create('Email sent', $description),
        );
      }
    };

    return $stack->next()->handle($envelope, $stack);
  }

  private function getStampForQueue(string $queueName, mixed $data): ?DescriptionStamp {
    return match ($queueName) {
      // \Drupal\media\Plugin\QueueWorker\ThumbnailDownloader.
      'media_entity_thumbnail' => (function (mixed $data): ?DescriptionStamp {
        $media = $this->mediaStorage()->load($data['id']);
        if ($media === NULL) {
          return NULL;
        }

        return DescriptionStamp::create(
          (string) new TranslatableMarkup('Thumbnail fetched'),
          (string) new TranslatableMarkup('Generated thumbail for @media', [
            '@media' => $media->label(),
          ]),
        );
      })($data),
      default => NULL,
    };
  }

  private function mediaStorage(): MediaStorage {
    /** @var \Drupal\media\MediaStorage */
    return $this->entityTypeManager()->getStorage('media');
  }

  private function entityTypeManager(): EntityTypeManagerInterface {
    return ($this->entityTypeManager)();
  }

}
